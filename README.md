//Creating a post using postman
1. Select Request Type "POST"
2. In Headers Tab, Select Key = "Content-Type" and Value = "application/json"
3. In Body Tab Select "raw". 
4. Input: http://localhost/phpapi/api/post/create.php 
Example of a raw data input for post:
{
    "title" : "Environmental Science",
    "body" : "Environmental Science now a days is top of the topic to discuss.",
    "author" : "Raihan",
    "category_id" : "1"
}

//Read single data of post
http://localhost/phpapi/api/post/read_single.php?id=1

//Read all data of post
http://localhost/phpapi/api/post/read.php

//Read all categories
http://localhost/phpapi/api/category/read.php

//Read single category
http://localhost/phpapi/api/category/read_single.php?id=1

//Creating new category
//Steps are same as Post
Input address: http://localhost/phpapi/api/category/create.php
Example of a raw data input for category:
{   
    "name" : "Anime"
}
