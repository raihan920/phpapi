<?php
//headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Category.php';

//DB connect and instantiate
$database = new Database();
$db = $database->connect();

//instantiate post object
$category = new Category($db);

//Get ID form user input
$category->id = isset($_GET['id']) ? $_GET['id'] : die();

//Get post
$category->read_single();

//create data array
$category_arr = array(
    'id' => isset($category->id) ? $category->id : "Not Found",
    'name' => isset($category->name) ? $category->name : "Not Found"
);

//JSON output
print_r(json_encode($category_arr));