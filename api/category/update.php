<?php
//headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Category.php';

//DB connect and instantiate
$database = new Database();
$db = $database->connect();

//instantiate post object
$category = new Category($db);

//Get raw posted data
$data = json_decode(file_get_contents("php://input"));

$category->id = $data->id;
$category->name = $data->name;

//post creation
if($category->update()){
    echo json_encode(
        array('message' => 'Category Updated Successfully!')
    );
} else {
    echo json_encode(
        array('message' => 'Sorry! Category Was Not Updated!')
    );
}