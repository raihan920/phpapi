<?php
//headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Category.php';

//DB connect and instantiate
$database = new Database();
$db = $database->connect();

//instantiate category object
$category = new Category($db);
//get the data
$result = $category->read();
//get row count
$num = $result->rowCount();

//if data found
if($num>0){
    //Post array
    $categories_arr = array();
    $categories_arr['data'] = array();
    while ($row = $result->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        
        $category_item = array(
            'id' => $id,
            'name' => $name            
        );        
        //push to data
        array_push($categories_arr['data'], $category_item);
    }
    //Turn to JSON and output
    echo json_encode($categories_arr);
} else {
    //No posts
    echo json_encode(
        array('message' => 'No Posts Found')
    );
}