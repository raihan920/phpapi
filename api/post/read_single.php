<?php
//headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Post.php';

//DB connect and instantiate
$database = new Database();
$db = $database->connect();

//instantiate post object
$post = new Post($db);

//Get ID 
$post->id = isset($_GET['id']) ? $_GET['id'] : die();

//Get post
$post->read_single();

//create data array
$post_arr = array(
    'id' => isset($post->id) ? $post->id : "Not Found",
    'title' => isset($post->title) ? $post->title : "Not Found",
    'body' => html_entity_decode(isset($post->body) ? $post->body : "Not Found"),
    'author' => isset($post->author) ? $post->author : "Not Found",
    'category_id' => isset($post->category_id) ? $post->category_id : "Not Found",
    'category_name' => isset($post->category_name) ? $post->category_name : "Not Found"
);

//JSON output
print_r(json_encode($post_arr));

    

