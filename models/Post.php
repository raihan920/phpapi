<?php
class Post{
    //DB property
    private $conn = '';
    private $table = 'posts';
    
    //post properties
    public $id;
    public $category_id;
    public $category_name;
    public $title;
    public $body;
    public $author;
    public $created_at;
    
    //DB connection constructor
    public function __construct($db) {
        $this->conn = $db;
    }
    
    //Get All Post
    public function read() {
        $query = "SELECT `c`.`name` AS `category_name`, `p`.`id`, `p`.`category_id`, `p`.`title`, `p`.`body`, `p`.`author`, `p`.`created_at` FROM `$this->table` `p` LEFT JOIN `categories` `c` ON `p`.`category_id` = `c`.`id` ORDER BY `p`.`created_at` DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }
    
    //Get Single Post
    public function read_single() {
        $query = "SELECT `c`.`name` AS `category_name`, `p`.`id`, `p`.`category_id`, `p`.`title`, `p`.`body`, `p`.`author`, `p`.`created_at` FROM `$this->table` `p` LEFT JOIN `categories` `c` ON `p`.`category_id` = `c`.`id` WHERE `p`.`id` = ? LIMIT 1";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $this->title = $row['title'];
            $this->body = $row['body'];
            $this->author = $row['author'];
            $this->category_id = $row['category_id'];
            $this->category_name = $row['category_name'];
        }        
    }
    
    //insert data
    public function create() {
        $query = "INSERT INTO `$this->table`
                    SET
                        `title` = :title,
                        `body` = :body,
                        `author` = :author,
                        `category_id` = :category_id";
        $stmt = $this->conn->prepare($query);
        //sanitize input
        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->body = htmlspecialchars(strip_tags($this->body));
        $this->author = htmlspecialchars(strip_tags($this->author));
        $this->category_id = htmlspecialchars(strip_tags($this->category_id));
        
        //bind data
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':body', $this->body);
        $stmt->bindParam(':author', $this->author);
        $stmt->bindParam(':category_id', $this->category_id);
        
        //execute
        if($stmt->execute()){            
            return true;
        } else {
            printf("Error: %s.\n", $stmt->error);
            return false;
        }        
    }
    
    //update post
    public function update() {
        $query = "UPDATE `$this->table`
                    SET
                `title` = :title,
                `body` = :body,
                `author` = :author,
                `category_id` = :category_id 
                WHERE `id` = :id";
        $stmt = $this->conn->prepare($query);
        //sanitize input
        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->body = htmlspecialchars(strip_tags($this->body));
        $this->author = htmlspecialchars(strip_tags($this->author));
        $this->category_id = htmlspecialchars(strip_tags($this->category_id));
        $this->id = htmlspecialchars(strip_tags($this->id));
        
        //bind data
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':body', $this->body);
        $stmt->bindParam(':author', $this->author);
        $stmt->bindParam(':category_id', $this->category_id);
        $stmt->bindParam(':id', $this->id);
        
        //execute
        if($stmt->execute()){            
            return true;
        } else {
            printf("Error: %s.\n", $stmt->error);
            return false;
        }        
    }
}