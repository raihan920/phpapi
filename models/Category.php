<?php
class Category {
    //DB property
    private $conn = '';
    private $table = 'categories';
    
    //category properties
    public $id;
    public $name;
    public $created_at;
    
    //db connect
    public function __construct($db) {
        $this->conn = $db;
    }
    
    //get all category
    public function read() {
        $query = "SELECT `c`.`id`, `c`.`name`, `c`.`created_at` FROM `$this->table` `c` ORDER BY `c`.`created_at` DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }
    
    //get single category
    public function read_single() {
        $query = "SELECT `c`.`id`, `c`.`name`, `c`.`created_at` FROM `$this->table` `c` WHERE `c`.`id` = ? LIMIT 1";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $this->name = $row['name'];            
        }        
    }
    
    //insert data
    public function create() {
        $query = "INSERT INTO `$this->table` SET `name` = :name";
        $stmt = $this->conn->prepare($query);
        //sanitize input
        $this->name = htmlspecialchars(strip_tags($this->name));        
        //bind data
        $stmt->bindParam(':name', $this->name);        
        //execute
        if($stmt->execute()){            
            return true;
        } else {
            printf("Error: %s.\n", $stmt->error);
            return false;
        }        
    }
    
    //update post
    public function update() {
        $query = "UPDATE `$this->table` SET `name` = :name WHERE `id` = :id";
        $stmt = $this->conn->prepare($query);
        //sanitize input
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->id = htmlspecialchars(strip_tags($this->id));        
        //bind data
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':id', $this->id);        
        //execute
        if($stmt->execute()){            
            return true;
        } else {
            printf("Error: %s.\n", $stmt->error);
            return false;
        }        
    }
}
